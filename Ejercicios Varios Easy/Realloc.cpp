#include <stdlib.h>
#include <stdio.h>


#define N 100

int
si_quiere_el_user (int s, int *room)
{
        return room [s-1] <  0 ? 0 : 1;
}

int
main (int argc, char *argv[])
{
        /*DECLARACION DE VARIABLES*/

        int *room;
        int summit = 0;

        room = (int *) malloc (sizeof (int));

        /*ENTRADA DE DATOS*/
        do{
           printf ("Número: ");
           scanf ("%i", &room[summit++]);
           room = (int *) realloc ( room, (summit +1) * sizeof (int ) ); /*El room es lo que queremos que cambie, si este valor no esta se ejecutará como un malloc*/
        } while (si_quiere_el_user (summit, room));
        summit--;

        /*SALIDA DE DATOS*/
        printf ("\n");
        for (int i=0; i<summit; i++)            
                printf ("%i: %i\n", i+1, room[i]);
                printf ("\n");


        free (room);

        return EXIT_SUCCESS;
}
