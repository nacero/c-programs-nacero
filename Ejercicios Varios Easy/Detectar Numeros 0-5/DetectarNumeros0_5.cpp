#include <stdio.h>
#include <stdlib.h>
//Este programa detecta los ceros y cincos que se introduzcan.
int main (){
	int cant, num;
	int ceros = 0;
	int cincos = 0;
	printf("Elige una cantidad de numeros:");
	scanf("%i", &cant);
	
	for(int i=0; i<cant; i++){
		printf("%i-Numero:", i+1);
		scanf("%i", &num);
	
	if(num == 0){
		ceros += 1;
	} 
	else if (num == 5){
		cincos += 1;
	}
}
	printf("El numero total de ceros introducido es de %i \n", ceros);
	printf("El numero total de cincos introducido es de %i \n", cincos);

	
	return EXIT_SUCCESS;
}
