#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

void
imprimir (double operacion) {
        printf ("Tu media es de:%lf\n", operacion);
}

int
main (int argc, char *argv[])
{
        double numeros;
        int suma = 0;
        int cont = 0;
        char operador[10];
        double convertido;

        printf("Notas a calcular:\n");

        do {
                printf ("Número:");
                __fpurge (stdin);
                cont = scanf("%[0-9]", operador);
                convertido = atof (operador);

                if (cont) {
                        numeros++;
                        suma+= convertido;

                }

        } while(cont);

                imprimir ( (double) suma/numeros);


        return EXIT_SUCCESS;

}
